# Screen Scenario:
Assuming that you will work on one screen app that is heavily depending on forms, your job in this task is to :

Collect Data from user through the form Make front end validation in the form fields
Do asynchronous form validation ( connecting to backend Apis) and check the value validty and show error message from the backend to the user

Send Button is Shown only if the Form is Valid and all Color Validation are correct other wise it is dimmed
Show automatic text Suggestions in one of the form fields from the backend data Finally you have to print the form

# Important Notes:
It is preferred to use a good state management solution and make a good Separation of concerns and follow good coding style we will evaluate you based on that.
Any UI will be accepted
at least one unit test for the form validation cases is required

#  Bonus : if you showed your ability to write only one widget test for one of the form fields


## How you will approach the task !!!
1. Inside This File you will see the Json File , copy and paste it and add it to your assets folder in your flutter project
2. you will need to get your data from asset file assuming that the data is coming from backend server
3. It is expected from you to have 4 TextFields covering all our previously discussed cases
4. Send the Form to separate function and print it when it is valid
5. Add button to navigate to new Screen
6. Press back button and show default assigned data


## Ui Deep Structure :
- First TextField you have to make validation that the text is more than 5 characters, Text is only allowed and max 9 Characters

- If First TextField starts with “a” hide automatically the second Text Field

- Create a separate function that returns asyncValidationColors array in the Json file , then validate “ error ”    field inside it if it true you have to show the corresponding errorMessage field

- Make a Third Text Field and allow the user to see Real time suggestions when entering data inside this field (AutoComplete inside this textField) you will need to parse “autoSuggestions” data and make autoComplete accordingly to the returned colors You will have to show the autocomplete color from the returned data inside the json

- Make a forth TextField to assign the parsed “red” data from the json file inside the textField directly by default at the start of the app

- Add TextButton that navigate to new Screen

- Press Back button and show the assigned default data


# Notes :
Asynchronous Validation : is used to validate our form through external server it is another way of form validation we need to show the validation message in real time
Assume that the function that you created that returned the error is working asynchronously you can make it Future and add Delay timer to mock the situation
