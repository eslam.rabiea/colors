enum RequestTypeResponse { Pending, Approved, Declined, Canceled }
enum AsyncValidationStatus { Pending, Valid, Invalid }
