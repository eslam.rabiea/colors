import 'dart:convert';

import 'package:flutter/widgets.dart';

class ReadingAssets {
  static late final String _appFiles;

  static Future<void> loadAssets(BuildContext context) async {
    _appFiles =
        await DefaultAssetBundle.of(context).loadString('AssetManifest.json');
  }

  static List<String> getListAssets(String path) {
    return json
        .decode(_appFiles)
        .keys
        .where((String key) => key.startsWith(path))
        .toList() as List<String>
      ..removeWhere((val) => val.contains('DS_Store'));
  }
}
