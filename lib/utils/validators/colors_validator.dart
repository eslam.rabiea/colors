import 'package:rasan_colors/models/color_validation.dart';

abstract class ColorsValidatorsAb {
  String validate();
}

class ColorsValidator with ColorsValidatorsAb {
  ColorsValidator(this.validatorsList);
  final List<ColorValidationModel> validatorsList;

  @override
  String validate() {
    String validChecker = 'valid';
    return validChecker;
  }
}
