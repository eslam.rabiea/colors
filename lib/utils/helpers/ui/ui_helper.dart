import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:rasan_colors/config/theme/text_styles.dart';
import 'package:rasan_colors/utils/constants/colors.dart';

class UI {
  static OverlaySupportEntry showMessage(String message,
      [bool success = false]) {
    return showSimpleNotification(
        Text(
          message,
          style: AppTextStyles.body1,
        ),
        contentPadding: EdgeInsets.all(8),
        elevation: 3,
        background: success
            ? AppColors.primaryColor.withOpacity(.8)
            : Colors.redAccent.withOpacity(.8));
  }
}
