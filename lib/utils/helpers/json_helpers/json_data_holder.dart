class JsonDataHolder {
  JsonDataHolder._privateConstructor();
  static final JsonDataHolder instance = JsonDataHolder._privateConstructor();

  void init(Map<String, dynamic> json) {
    this._json = json;
  }

  late final Map<String, dynamic> _json;

  String getRedColorAutoAssigned() => _json['red'];

  List<Map<String, dynamic>> getAsyncValidationColors() =>
      List.of(_json['groupOfColors']['asyncValidationColors'])
          .map((e) => Map<String, dynamic>.from(e))
          .toList();

  List<String> getAutoSuggestionsColors() =>
      List.of(_json['groupOfColors']['autoSuggestionsColors'])
          .map((e) =>e.toString())
          .toList();
}
