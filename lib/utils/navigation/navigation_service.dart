import 'package:flutter/material.dart';
import 'transitions/custom_transition_two.dart';

class Nav {
  static Future<dynamic> push(BuildContext context, Widget page,
      {bool replace = false}) async {
    final route = MaterialPageRoute(builder: (c) => page);

    return replace
        ? await Navigator.pushReplacement(context, route)
        : await Navigator.push(context, route);
  }

  static Future<dynamic> pushTransparent(
    BuildContext context,
    Widget page,
  ) async {
    return await Navigator.of(context).push(
      PageRouteBuilder(
        opaque: false, // set to false
        pageBuilder: (_, __, ___) => page,
      ),
    );
  }

  static Future<dynamic> pushAnimation(BuildContext context, Widget page,
      {bool replace = false}) async {
    Route<Object?> route;

    route = CustomTransition(builder: (c) => page);

    return replace
        ? await Navigator.pushReplacement(context, route)
        : await Navigator.push(context, route);
  }

  static Future<dynamic> pushReplaceAll(
    BuildContext context,
    Widget page,
  ) async {
    return Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => page),
        (Route<dynamic> route) => false);
  }

  static Future<dynamic> pushReplaceAllNamed(
    BuildContext context,
    String newRouteName,
  ) async {
    return await Navigator.of(context)
        .pushNamedAndRemoveUntil(newRouteName, (route) => false);
  }
}
