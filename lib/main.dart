import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_builder_validators/localization/l10n.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:rasan_colors/views/splash/splash_screen.dart';
import 'config/getit/getit_setup.dart';
import 'config/responsiveness/size_config.dart';
import 'config/shared_pref/preference.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Preference.init();
  setupLocator();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(const RasanColors());
}

class RasanColors extends StatelessWidget {
  const RasanColors({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return OverlaySupport.global(
      child: MultiProvider(
        providers: [...providers],
        child: MaterialApp(
          title: 'Rasan Colors',
          // showPerformanceOverlay: true,
          debugShowCheckedModeBanner: false,
          home: LayoutBuilder(
            builder: (context, constraints) {
              SizeCfg().init(constraints);
              return const SplashScreen();
            },
          ),
          localizationsDelegates: [
            // GlobalMaterialLocalizations.delegate,
            // GlobalWidgetsLocalizations.delegate,
            FormBuilderLocalizations.delegate,
          ],
        ),
      ),
    );
  }
}
