import 'dart:convert';

ColorValidationModel colorValidationFromJson(String str) =>
    ColorValidationModel.fromJson(json.decode(str));

class ColorValidationModel {
  ColorValidationModel({
    this.color,
    this.error,
    this.errorMessage,
  });

  String? color;
  String? error;
  String? errorMessage;

  factory ColorValidationModel.fromJson(Map<String, dynamic> json) =>
      ColorValidationModel(
        color: json["color"].toString(),
        error: json["error"].toString(),
        errorMessage: json["errorMessage"].toString(),
      );
}
