import 'dart:convert';

import 'package:rasan_colors/utils/constants/enums.dart';
import 'package:rasan_colors/utils/helpers/json_helpers/json_data_holder.dart';

AsyncFormValidator asyncFormValidatorFromJson(String str) =>
    AsyncFormValidator.fromJson(json.decode(str));

class AsyncFormValidator {
  AsyncFormValidator(
      {this.color,
      this.error,
      this.errorMessage,
      this.asyncValidationStatus = AsyncValidationStatus.Valid});

  String? color;
  String? error;
  String? errorMessage;
  AsyncValidationStatus asyncValidationStatus;

  factory AsyncFormValidator.fromJson(Map<String, dynamic> json) =>
      AsyncFormValidator(
        color: json["color"].toString(),
        error: json["error"].toString(),
        errorMessage: json["errorMessage"].toString(),
      );
}

class DataRepo {
  Future<Iterable<AsyncFormValidator>> getData() async {
    // delay assumption
    await Future.delayed(Duration(seconds: 2));

    // assuming here that we are got the data
    return JsonDataHolder.instance
        .getAsyncValidationColors()
        .map((e) => AsyncFormValidator.fromJson(e));
  }
}
