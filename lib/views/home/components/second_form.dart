import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:rasan_colors/controllers/data.c.dart';
import 'package:rasan_colors/views/widgets.dart/form_textfield.dart';

class SecondForm extends StatelessWidget {
  const SecondForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Selector<DataController, bool>(
        selector: (_, cont) => cont.isFirstFormStartsWithLetterA,
        builder: (context, isFirstFormStartsWithLetterA, __) {
          return Visibility(
              visible: !isFirstFormStartsWithLetterA,
              child: FormTextField(
                controllerName: secondFormKey,
                hint: 'Will be hidden if First starts with a',
                validators: FormBuilderValidators.compose([
                  FormBuilderValidators.required(context),
                ]),
              ));
        });
  }
}
