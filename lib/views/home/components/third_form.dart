import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rasan_colors/controllers/data.c.dart';
import 'package:rasan_colors/views/widgets.dart/form_autoComplete.dart';

class ThirdForm extends StatelessWidget {
  const ThirdForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = context.read<DataController>();
    return TextFiledAutoComplete(
        autoCompleteData: controller.autoCompleteWords,
        controllerName: thirdFormKey,
        hint: 'Auto Complete Field');
  }
}
