import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/src/provider.dart';
import 'package:rasan_colors/controllers/data.c.dart';
import 'package:rasan_colors/models/async_form_validator.dart';
import 'package:rasan_colors/utils/constants/enums.dart';
import 'package:rasan_colors/utils/helpers/ui/ui_helper.dart';
import 'package:rasan_colors/views/widgets.dart/form_textfield.dart';

class FirstForm extends StatelessWidget {
  const FirstForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = context.read<DataController>();
    return StreamBuilder<AsyncFormValidator>(
        stream: controller.stream,
        builder: (context, snapshot) {
          if (snapshot.data?.asyncValidationStatus ==
              AsyncValidationStatus.Invalid) {
            SchedulerBinding.instance?.addPostFrameCallback((_) async {
              UI.showMessage(snapshot.data?.errorMessage ?? "");
            });
          }
          return FormTextField(
            controllerName: firstFormKey,
            hint: 'Enter color',
            isLoadingData: snapshot.data?.asyncValidationStatus ==
                AsyncValidationStatus.Pending,
            onChange: (value) =>
                controller.onChangeFirstField(value, DataRepo().getData),
            validators: FormBuilderValidators.compose([
              FormBuilderValidators.maxLength(context, 9),
              FormBuilderValidators.minLength(context, 5),
              FormBuilderValidators.required(context),
            ]),
          );
        });
  }
}
