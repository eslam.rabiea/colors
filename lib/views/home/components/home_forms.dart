import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:rasan_colors/config/responsiveness/size_config.dart';
import 'package:rasan_colors/controllers/data.c.dart';
import 'package:rasan_colors/utils/helpers/json_helpers/json_data_holder.dart';
import 'package:rasan_colors/utils/navigation/navigation_service.dart';
import 'package:rasan_colors/views/home/components/second_form.dart';
import 'package:rasan_colors/views/home/components/third_form.dart';
import 'package:rasan_colors/views/next/next_page.dart';
import 'package:rasan_colors/views/widgets.dart/form_autoComplete.dart';
import 'package:rasan_colors/views/widgets.dart/form_textfield.dart';

import '../../widgets.dart/action_button.dart';
import 'first_form.dart';

class HomeForms extends StatefulWidget {
  const HomeForms({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeForms> createState() => _HomeFormsState();
}

class _HomeFormsState extends State<HomeForms> {
  @override
  void initState() {
    context.read<DataController>().assignAutoCompleteData(
        JsonDataHolder.instance.getAutoSuggestionsColors());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final controller = context.read<DataController>();
    return Container(
      height: SizeCfg.heightMultiplier * 100,
      width: SizeCfg.widthMultiplier * 100,
      child: Center(
        child: FormBuilder(
          key: controller.formController,
          onChanged: controller.onChangeForms,
          initialValue: {
            thirdFormKey: null,
            fourthFormKey: JsonDataHolder.instance.getRedColorAutoAssigned()
          },
          child: SingleChildScrollView(
            child: Column(
              children: [
                // FIRST
                const FirstForm(),
                // SECOND
                const SecondForm(),
                // THIRD
                const ThirdForm(),
                // FOURTH
                FormTextField(
                  controllerName: fourthFormKey,
                  hint: 'Default Value',
                  validators: FormBuilderValidators.compose([
                    FormBuilderValidators.required(context),
                  ]),
                ),

                // BUTTON
                SizedBox(
                  height: SizeCfg.heightMultiplier * 5,
                ),
                Consumer<DataController>(
                  builder: (context, cont, _) => Container(
                    height: SizeCfg.heightMultiplier * 6,
                    width: SizeCfg.widthMultiplier * 60,
                    child: ActionButton(
                      text: 'Print',
                      key: Key('MainButton'),
                      onPressed: !cont.isFormValid()
                          ? null
                          : () {
                              Logger().w(controller
                                  .formController.currentState?.value);
                            },
                    ),
                  ),
                ),
                SizedBox(
                  height: SizeCfg.heightMultiplier * 30,
                ),
                const _NextButton()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _NextButton extends StatelessWidget {
  const _NextButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        height: SizeCfg.heightMultiplier * 5,
        width: SizeCfg.widthMultiplier * 35,
        margin: EdgeInsets.all(15),
        child: ActionButton(
          text: 'Next Page',
          onPressed: () {
            Nav.pushAnimation(context, const NextPage());
            context.read<DataController>().clearState();
          },
          backgroundColor: Colors.black54,
        ),
      ),
    );
  }
}
