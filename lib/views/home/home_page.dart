import 'package:flutter/material.dart';
import 'package:rasan_colors/utils/constants/paths.dart';
import '../widgets.dart/frozen_container.dart';
import 'components/home_forms.dart';

class AppHomePage extends StatelessWidget {
  const AppHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              Paths.homeImage,
              fit: BoxFit.cover,
            ),
            const FrozenContainer(),
            const HomeForms(),
          ],
        ),
      ),
    );
  }
}
