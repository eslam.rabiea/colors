import 'package:flutter/material.dart';
import 'package:rasan_colors/utils/constants/paths.dart';

class SplashLogo extends StatelessWidget {
  const SplashLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(Paths.splashImage);
  }
}
