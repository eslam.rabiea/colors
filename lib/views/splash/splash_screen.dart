import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:rasan_colors/config/responsiveness/size_config.dart';
import 'package:rasan_colors/utils/constants/colors.dart';
import 'package:rasan_colors/utils/constants/paths.dart';
import 'package:rasan_colors/utils/helpers/json_helpers/json_data_holder.dart';
import 'package:rasan_colors/utils/helpers/json_helpers/json_uploader.dart';
import 'package:rasan_colors/utils/navigation/navigation_service.dart';
import 'package:rasan_colors/views/home/home_page.dart';

import 'components/splash_logo.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance?.addPostFrameCallback((_) async {
      JsonDataHolder.instance
          .init(await JsonUploader.parseJsonFromAssets(Paths.jsonFile));
      await Future.delayed(Duration(seconds: 1));
      Nav.pushReplaceAll(context, const AppHomePage());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          color: AppColors.primaryColor,
          height: SizeCfg.heightMultiplier * 100,
          width: SizeCfg.widthMultiplier * 100,
          alignment: Alignment.center,
          child: const SplashLogo(),
        ));
  }
}
