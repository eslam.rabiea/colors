import 'dart:ui';

import 'package:flutter/material.dart';

class FrozenContainer extends StatelessWidget {
  const FrozenContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRect(
      child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 20.0, sigmaY: 30.0),
          child: Container(
            color: Colors.white.withOpacity(.8),
          )),
    );
  }
}
