import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:rasan_colors/config/theme/decoration.dart';
import 'package:rasan_colors/config/theme/text_styles.dart';

class TextFiledAutoComplete extends StatelessWidget {
  final String controllerName;
  final String hint;
  final bool withBorder;
  final Function(dynamic)? onChange;
  final String? Function(dynamic)? validators;
  final dynamic initialValue;
  final double? height;
  final List<String> autoCompleteData;
  final EdgeInsetsGeometry innerPadding;
  final Function()? onReset;

  final double? padding;
  const TextFiledAutoComplete(
      {Key? key,
      required this.controllerName,
      required this.autoCompleteData,
      required this.hint,
      this.onReset,
      this.withBorder = true,
      this.initialValue,
      this.validators,
      this.innerPadding = const EdgeInsets.symmetric(horizontal: 8),
      this.onChange,
      this.padding,
      this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(padding ?? 8.0),
      child: Container(
        // height: height ?? 5.h,
        child: FormBuilderField(
          name: controllerName,
          validator: validators,
          onReset: onReset,
          // tristate: true,
          builder: (field) {
            return Autocomplete<String>(
              fieldViewBuilder: (context, textEditingController, focusNode,
                      onFieldSubmitted) =>
                  TextField(
                controller: textEditingController,
                focusNode: focusNode,
                style: AppTextStyles.body2.copyWith(color: Colors.black),
                decoration: InputDecoration(
                    contentPadding: innerPadding,
                    border: OutlineInputBorder(
                      borderRadius: AppDecoration.defaultRadius,
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    hintStyle:
                        AppTextStyles.body2.copyWith(color: Colors.black45),
                    hintText: hint),
              ),
              optionsBuilder: (TextEditingValue textEditingValue) {
                if (textEditingValue.text == '') {
                  return const Iterable<String>.empty();
                }
                return autoCompleteData.where((String option) {
                  return option.startsWith(textEditingValue.text);
                });
              },
              onSelected: (String selection) {
                field.didChange(selection);
              },
            );
          },
          onChanged: onChange,
          // textAlignVertical: TextAlignVertical.bottom,
          decoration: InputDecoration(
              contentPadding: innerPadding,
              border: OutlineInputBorder(
                borderRadius: AppDecoration.defaultRadius,
                borderSide: BorderSide(width: 2, color: Colors.black),
              ),
              hintStyle: AppTextStyles.body2.copyWith(color: Colors.black45),
              hintText: hint),
          enabled: initialValue == null ? true : false,
        ),
      ),
    );
  }
}
