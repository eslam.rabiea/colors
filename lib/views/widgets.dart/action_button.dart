import 'package:flutter/material.dart';
import 'package:rasan_colors/config/theme/text_styles.dart';
import 'package:rasan_colors/utils/constants/colors.dart';

class ActionButton extends StatelessWidget {
  final Color? backgroundColor;
  final TextStyle? textStyle;
  final Function()? onPressed;
  final String text;
  const ActionButton(
      {Key? key,
      required this.text,
      this.textStyle,
      this.backgroundColor,
      this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: textStyle ?? AppTextStyles.button1.copyWith(color: Colors.white),
      ),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled))
              return AppColors.primaryColor.withOpacity(.4);
            return Colors.green;
          },
        ),
      ),
    );
  }
}
