import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:rasan_colors/config/theme/decoration.dart';
import 'package:rasan_colors/config/theme/text_styles.dart';
import 'package:rasan_colors/utils/constants/colors.dart';

class FormTextField extends StatelessWidget {
  final String controllerName;
  final double? height;
  final String? initialValue;
  final int maxLines;
  final String hint;
  final String type;
  final isLoadingData;
  final String? Function(String?)? validators;
  final EdgeInsetsGeometry innerPadding;
  final EdgeInsetsGeometry outerPadding;
  final Function(String?)? onChange;
  const FormTextField(
      {Key? key,
      required this.controllerName,
      required this.hint,
      this.type = 'none',
      this.initialValue,
      this.isLoadingData = false,
      this.onChange,
      this.innerPadding = const EdgeInsets.symmetric(horizontal: 8),
      this.outerPadding = const EdgeInsets.all(8),
      this.maxLines = 1,
      this.validators,
      this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: outerPadding,
      child: Container(
        // height: height ?? 5.h,
        width: double.infinity,

        child: Row(
          children: [
            Expanded(
              child: FormBuilderTextField(
                name: controllerName,
                validator: validators,
                initialValue: initialValue,
                maxLines: maxLines,
                enableSuggestions: true,
                onChanged: onChange,
                keyboardType: TextInputType.text,
                style: AppTextStyles.body2.copyWith(color: Colors.black),
                decoration: InputDecoration(
                    contentPadding: innerPadding,
                    border: OutlineInputBorder(
                      borderRadius: AppDecoration.defaultRadius,
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    hintStyle:
                        AppTextStyles.body2.copyWith(color: Colors.black45),
                    hintText: hint),
              ),
            ),
            if (isLoadingData)
              Container(
                padding: innerPadding,
                child: CircularProgressIndicator(
                  backgroundColor: Colors.grey,
                  color: AppColors.primaryColor,
                ),
              )
          ],
        ),
      ),
    );
  }
}
