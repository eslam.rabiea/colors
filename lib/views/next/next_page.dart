import 'package:flutter/material.dart';
import 'package:rasan_colors/utils/constants/paths.dart';
import 'package:rasan_colors/views/widgets.dart/action_button.dart';
import '../widgets.dart/frozen_container.dart';

class NextPage extends StatelessWidget {
  const NextPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              Paths.homeImage,
              fit: BoxFit.cover,
            ),
            const FrozenContainer(),
            Center(
              child: ActionButton(
                text: 'Back',
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
