import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:rasan_colors/config/getit/base_notifier.dart';
import 'package:rasan_colors/models/async_form_validator.dart';
import 'package:rasan_colors/utils/constants/enums.dart';

// USED TO IDENTIFY EVERY FORM
const String firstFormKey = 'firstFormKey';
const String secondFormKey = 'secondFormKey';
const String thirdFormKey = 'thirdFormKey';
const String fourthFormKey = 'fourthFormKey';

typedef Future<Iterable<AsyncFormValidator>> GettingDataFromApi();

/// data controller is responsable for all forms operation
class DataController extends BaseNotifier {
  DataController() {
    stream = _fieldStreamController.stream;
  }
  // AlL Forms Controller
  final formController = GlobalKey<FormBuilderState>();

  // toggle the second form based on first form
  bool _isFirstFormStartsWithLetterA = false;
  bool get isFirstFormStartsWithLetterA => _isFirstFormStartsWithLetterA;

  // handles async validation status
  bool _isAsyncFormValid = false;
  bool get isAsyncFormValid => _isAsyncFormValid;

  // handles all forms state
  bool _isFormValid = false;

  // auto complete words that gotten from backend
  // assigned only with
  late final List<String> autoCompleteWords;

  // streams used to streaming over every text that user type on textfield to working with async validation
  final _fieldStreamController = StreamController<AsyncFormValidator>();
  late final Stream<AsyncFormValidator> stream;

  void assignAutoCompleteData(List<String> data) {
    autoCompleteWords = data;
  }

  void onChangeForms() async {
    print("FORM VALIDATION ${formController.currentState?.saveAndValidate()}");
    _isFormValid = formController.currentState?.saveAndValidate() ?? false;
    setState();
  }

  Future<void> onChangeFirstField(String? val,
      [GettingDataFromApi? data]) async {
    if (val != null && val.trim().isNotEmpty) {
      _checkIsFirstFormStartsWithLetterA(val);

      // checking here to not run queries when tha frontend validation is invalid
      if (val.length >= 4 && val.length <= 9 && data != null) {
        await _validateAsyncForm(val, data);
      }
      setState();
    }
  }

  void clearState() {
    formController.currentState?.reset();
    _isFirstFormStartsWithLetterA = false;
    _isAsyncFormValid = false;
    _isFormValid = false;
  }

  Future<void> _validateAsyncForm(String value, GettingDataFromApi data) async {
    _fieldStreamController.add(AsyncFormValidator(
        asyncValidationStatus: AsyncValidationStatus.Pending));

    // assuming here that we are got the data
    final mappedValidators = await data();

    // delay assumption
    await Future.delayed(Duration(seconds: 2));

    final bool isAnyValidatorsMatchTextField =
        mappedValidators.any((val) => val.color == value);

    if (isAnyValidatorsMatchTextField) {
      // got invalid validation
      final element = mappedValidators.firstWhere((data) => data.color == value)
        ..asyncValidationStatus = AsyncValidationStatus.Invalid;
      _fieldStreamController.add(element);
      _isAsyncFormValid = false;
    } else {
      // valid validation
      _fieldStreamController.add(AsyncFormValidator(
          asyncValidationStatus: AsyncValidationStatus.Valid));
      _isAsyncFormValid = true;
    }
  }

  void _checkIsFirstFormStartsWithLetterA(String firstForm) {
    if (firstForm.startsWith('a') || firstForm.startsWith('A')) {
      _isFirstFormStartsWithLetterA = true;
    } else {
      _isFirstFormStartsWithLetterA = false;
    }
    setState();
  }

  bool isFormValid() {
    return _isFormValid && _isAsyncFormValid;
  }
}
