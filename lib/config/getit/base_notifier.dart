import 'package:flutter/material.dart';

enum NotifierState { busy, idle, error }

class BaseNotifier extends ChangeNotifier {
  NotifierState _state = NotifierState.idle;
  bool _mounted = true;
  // AuthenticationService get auth => locator<AuthenticationService>();

  bool get mounted => _mounted;
  NotifierState get state => _state;
  bool get idle => _state == NotifierState.idle;
  bool get busy => _state == NotifierState.busy;
  bool get hasError => _state == NotifierState.error;

  void setBusy() => setState(state: NotifierState.busy);
  void setIdle() => setState();
  void setError() => setState(state: NotifierState.error);

  void setState(
      {NotifierState state = NotifierState.idle, bool notifyListener = true}) {
    _state = state;
    if (_mounted && notifyListener) {
      notifyListeners();
      // Logger().v('SETSTATE FROM ${this.runtimeType}');
    }
  }

  @override
  void dispose() {
    _mounted = false;
    debugPrint(
        '*************** \n\n -- ${this.runtimeType} Provider Has Disposed -- \n\n***************');
    super.dispose();
  }
}
