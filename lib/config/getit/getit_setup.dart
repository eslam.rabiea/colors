import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:rasan_colors/controllers/data.c.dart';

final GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => DataController());
}

List<SingleChildWidget> providers = [
  ...independentServices,
  // ...nonIndependentServices
];

List<SingleChildWidget> independentServices = [
  ChangeNotifierProvider<DataController>(
      create: (_) => locator<DataController>()),
];
