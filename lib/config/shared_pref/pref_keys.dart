class PrefKeys {
  static const String TOKEN = 'token';
  static const String TOKEN_EXP_TIMESTAMP = 'tokenExpTimestamp';
  static const String USER_DATA = 'userData';
  static const String DOMAIN_NAME = 'domain';
  static const String SHOW_TUTORIAL = 'show-tutorial';
}
