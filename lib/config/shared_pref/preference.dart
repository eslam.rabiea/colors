// ignore_for_file: constant_identifier_names, always_put_control_body_on_new_line

import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Preference {
  Preference._internal();
  static late final SharedPreferences sb;
  static Future<void> init() async {
    sb = await SharedPreferences.getInstance();
  }

  static String? getString(String key) {
    try {
      return sb.getString(key);
    } catch (e) {
      return null;
    }
  }

  static List<String>? getStringList(String key) {
    try {
      return sb.getStringList(key);
    } catch (e) {
      return null;
    }
  }

  static int? getInt(String key) {
    try {
      return sb.getInt(key);
    } catch (e) {
      return null;
    }
  }

  static bool? getBool(String key) {
    try {
      return sb.getBool(key);
    } catch (e) {
      return null;
    }
  }

  static Future<bool?> setString(String key, String? value) async {
    if (value == null) return false;
    try {
      return sb.setString(key, value);
    } catch (e) {
      return null;
    }
  }

  static Future<bool?> setStringList(String key, List<String>? value) async {
    if (value == null) return false;
    try {
      return sb.setStringList(key, value);
    } catch (e) {
      return null;
    }
  }

  static Future<bool?> setInt(String key, int? value) async {
    if (value == null) return false;
    try {
      return sb.setInt(key, value);
    } catch (e) {
      return null;
    }
  }

  static Future<bool?> setBool(String key, bool value) async {
    try {
      return await sb.setBool(key, value);
    } catch (e) {
      return null;
    }
  }

  static Future<bool?> remove(String key) async {
    try {
      return await sb.remove(key);
    } catch (e) {
      return null;
    }
  }

  static Future<bool?> clear() async {
    try {
      return await sb.clear();
    } catch (e) {
      Logger().e(e);
      return null;
    }
  }
}
