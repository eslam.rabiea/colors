import 'package:flutter/material.dart';

class AppTextStyles {
  static final AppTextStyles _singleton = AppTextStyles._internal();
  AppTextStyles._internal();
  factory AppTextStyles() {
    return _singleton;
  }

  /// `36` - Font weight `700`
  static TextStyle header1 =
      TextStyle(fontSize: 36, fontWeight: FontWeight.w700, color: Colors.white);

  /// `32` - Font weight `700`
  static TextStyle header2 =
      TextStyle(fontSize: 32, fontWeight: FontWeight.w700, color: Colors.white);

  /// `24` - Font weight `700`
  static TextStyle body1 =
      TextStyle(fontSize: 24, fontWeight: FontWeight.w700, color: Colors.white);

  /// `20`- Font weight `700`
  static TextStyle body2 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w700,
  );

  /// `16` Font weight `400`
  static TextStyle body3 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );

  /// `12` - Font weight `400`
  static TextStyle subBody1 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
  );

  /// `10` - Font weight `400`
  static TextStyle subBody2 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w400,
  );

  /// `8`
  static TextStyle subBody3 = TextStyle(
    fontSize: 9,
  );

  /// `17` - Font weight `600`
  static TextStyle button1 =
      TextStyle(fontSize: 17, fontWeight: FontWeight.w600);

  /// `15` - Font weight `500`
  static TextStyle button2 = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w500,
  );
}
