import 'package:flutter/material.dart';

class AppDecoration {
  static final AppDecoration _singleton = AppDecoration._internal();
  AppDecoration._internal();
  factory AppDecoration() {
    return _singleton;
  }

  static LinearGradient grayGradient = LinearGradient(colors: [
    Colors.grey,
    Colors.grey.withOpacity(.7),
  ]);

  static BoxBorder border = Border.all(color: Colors.green);

  // static LinearGradient blueGradientTopToBottom = LinearGradient(
  //     begin: Alignment.topCenter,
  //     end: Alignment.bottomCenter,
  //     colors: [
  //       Color(0xff124392),
  //       AppColors.PRIMARY_COLOR,
  //       AppColors.PRIMARY_COLOR.withOpacity(.8),
  //     ]);

  static BorderRadius defaultRadius = BorderRadius.circular(8);
}
